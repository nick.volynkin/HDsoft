<!--

-->

DLNA (Digital Living Network Alliance) is an organization of consumer electronics manufacturers
 for development of technology standards for interoperable digital devices.
DLNA develops standards for real-time transmitting and playback of digital media content (i.e images, music and video) 
among devices in a home network. 


# History

<!--
В альянс, поддерживающий стандарты, изначально вошли Intel, Sony, Matsushita, Microsoft, Nokia, Philips, Hewlett-Packard, Samsung,
 по состоянию на 2013 год в нём состоит более 200 производителей оборудования, среди которых Cisco, Huawei, LG, Motorola.
-->

The Alliance was founded by Intel, Sony, Matsushita, Microsoft, Nokia, Philips, Hewlett-Packard and Samsung.
In 2013 more than 200 electronics manufacturers, including Cisco, Huawei, LG and Motorola were its members.

In 2008 more than 200 million DLNA-compatible devices have been sold worldwide.
In 2009 there have been more than 5 thousand different DLNA-compatible devices.

# Overview

<!-- 
Устройства, которые поддерживают спецификацию DLNA, по желанию пользователя могут настраиваться и объединяться в сеть в автоматическом режиме.

В рамках альянса стандарты развиваются для обеспечения мультимедиа-обмена для широкого класса бытовых устройств
(телевизоров, видеокамер, аудио- и видеопроигрывателей, мобильных телефонов, компьютеров, принтеров и т. п.),
-->

<!--
Средой передачи медиаконтента обычно является домашняя локальная сеть (IP-сеть).
 Подключение DLNA-совместимых устройств к домашней сети может быть как проводным (Ethernet), так и беспроводным (Wi-Fi).
-->

The main idea and purpose of DLNA standards is to allow the consumer to easily join digital media devices into a single network.
The range of devices includes TV-sets, video cameras, audio and video players, mobile phones, PCs, printers, etc.
The digital media content is usually transmitted through a home IP network.
DLNA-compatible devices can use either wired (Ethernet) or wireless (Wi-FI) connection to the network.

<!--
в некотором смысле эти стандарты являются идеологическими наследниками так и не получившей широкое распространение технологии UPnP. 
В ней для обнаружения и управления устройствами и медиапотоками используются протоколы UPnP Audio and Video 1.0 и UPnP Device Architecture 1.0.
-->

DLNA standards can be seen as successors to the UPnP technology, 
which employed UPnP Audio and Video 1.0 and UPnP Device Architecture 1.0 protocols
to manage digital devices and media content.

<!--
Таким образом, в рамках DLNA описываются практически все сценарии совместного использования устройств. 
К примеру, можно напрямую с цифровой фотокамеры или мобильного телефона отобразить снимки на телевизоре или распечатать на принтере. 
А для того, чтобы посмотреть по телевизору загруженный из сети фильм, не нужно записывать его на компакт-диск. 
Достаточно запустить воспроизведение на компьютере, указав в качестве средства вывода телевизор, 
или загрузить этот фильм в сетевое хранилище и открыть при помощи видеопроигрывателя, подключенного к домашней сети.
-->

DLNA aims to cover all available scenarios of media devices used together.
For example, the customer can watch photos from a camera or a mobile phone directly on the TV or send them to a printer.
The customer can also download a movie to their PC and watch it on the TV by explicitly setting that as the output device.
Alternatively, they can download a movie to a network-attached storage and play it from their video player.

<!--
Каждое изделие, которое проходит DLNA-сертификацию, получает знак со специальной символикой.
-->

Each DLNA-compatible device is marked with the DLNA logo.  

# Device classes
## Home Network Devices

<!--
Для того, чтобы домашний персональный компьютер мог исполнять функции DLNA-совместимого устройства, 
необходима установка соответствующего программного обеспечения.
-->

This class includes various home electronic devices like network-attached storages, audio and video players, TVs, audio systems and printers. 
A personal computer can become a DLNA-compatible HND with the appropriate software.
A device can belong to several subclasses.

* Digital Media Server (DMS)
* Digital Media Player (DMP)
* Digital Media Controller (DMC)
* Digital Media Renderer (DMR)

## Mobile Handheld Devices

This class includes mobile phones, portable players, laptops, photo and video cameras.
MHDs are close to HNDs in functionality but have lower requirements for performance and supported media formats.

* Mobile Digital Media Server (M-DMS)
* Mobile Digital Media Player (M-DMP)
* Mobile Digital Media Downloader (M-DMD)
* Mobile Digital Media Uploader (M-DMP)
* Mobile Digital Media Controller (M-DMC)

## Home Infrastructure Devices

This class includes devices, that extend connectivity between other devices in the network 
and enable conversion between different media formats.
