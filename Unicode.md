# Contents

* [Unicode basics](#unicode-basics)
* [Encodings](#encodings)
    * [Self-synchronization](#self-synchronization)
    * [Endianness](#endianness)
    * [UCS-2](#ucs-2)
    * [UTF-16](#utf-16)
    * [UTF-8](#utf-8)
* [Unicode-aware development](#unicode-aware-development)
    * [Data flow and internal representation](#data-flow-and-internal-representation)
    * [You don’t have to implement all from scratch](#you-dont-have-to-implement-all-from-scratch)
    * [Myths and Caveats](#myths-and-caveats)
    * [Invalid Unicode](#invalid-unicode)
    * [User Experience](#user-experience)
* [About this document](#about-this-document)

# Unicode basics

*Unicode* is a standard for text encoding.
It has been developed in attempt to solve the problems of multiple language-dependent code pages and provide:

* Ability to encode any script (writing system);
* Enough codespace capacity to encode all scripts (of which hieroglyphic are the largest ones);
* Ability to combine any number of languages (i.e. scripts) in a single document.

Unicode has embraced scripts of most modern and a number of extinct languages and multiple sign sets. 

A *code point* is a single element of Unicode codespace.
It can be, among other options:

  * An abstract character of a script (a letter, a hieroglyph or a punctuation mark);
  * A group of characters (e.g. a ligature);
  * An element of a character (e.g. a diacritical mark);
  * A symbol (e.g an arrow or emoji);
  * A non-printable character (e.g. the non-breakable space);
  * Reserved for encoding purposes;
  * Reserved for private use.

A code point is referred to by using `U+` with a hexadecimal number of 4 or more digits with leading zeros, if required.
An example is `U+0020`, the space symbol.
Unicode codespace capacity is 1,114,112, i.e. maximum value is `U+10FFFF`.
This codespace is divided into 17 parts called *planes*, numbered 0 to 16, each containing 2<sup>16</sup> code points.
Plane number 0 contains characters of most modern languages and is called the Basic Multilingual Plane (BMP).

Unicode includes the [Unicode Character Database](http://www.unicode.org/ucd/), 
that stores data about code points, such as case mappings, combination properties, etc. 


# Encodings

*Unicode encodings* are algorithms (functions) that map Unicode code points to byte arrays and back.
Encodings are defined in standards, developed by Unicode consortium.

The most known and used encodings are:

* UCS-2: 2-byte Universal Character Set;
* UTF-16: 16-bit Unicode Transformation Format;
* UTF-8: 8-bit Unicode Transformation Format.

## Self-synchronization

*Self-synchronization* is a property of encoding to produce a uniquely decodable code.
For example, encoding X maps `a` to `00`, `b` to `01` and `c` to `10`.
So in `10 01 00` each 2-bit unit can be decoded back in a unique way.
But if first bit is lost, we get `00 10 0`, which is decoded as `ac` and either `a` or an error.
It X can be said that X is synchronized on 2-bit units and not synchronized on 1-bit units.

Self-synchronization is required for an application to read data files or streams from any point.
Also it is much easier to recover damaged data, if it is encoded with a self-synchronizing encoding.

##  Endianness

In some encodings there are two different byte orders, required to compensate for specifics of hardware and software architecture. 

* Big-endian, where 2-byte word `0x0020` is stored as `0x00`, `0x20`;
* Little-endian, where the same word is stored in reversed order, as `0x20`, `0x00`.

Two first bytes of a Unicode string can be the Byte Order Mark (BOM).
In big-endian strings it has the value of `0xFEFF` and in little-endian it reverses to `0xFFFE`.
Reversed value is a signal to swap all byte pairs back to the big-endian order.

## UCS-2

UCS-2 is the original Unicode encoding, now obsolete.
It uses 2-byte units to encode each code point and maps the code point to the same binary value.
Therefore the maximum encoded code point in UCS-2 is `U+FFFF` and it is unable to encode any code point outside BMP.

There is also the UTF-32, a similar 'same value' 4-byte encoding.
It is quite redundant and not a preferred choice for information exchange and storage.
However, it gets employed in algorithms where direct character indexation is crucial.


## UTF-16

UTF-16 has been developed as a replacement for UCS-2.
It is able to encode any code point by using 2 or 4 bytes: 

* Code points in ranges from `U+0000` to `U+D7FF` and `U+E000` to `U+FFFF` are encoded with a single 2-byte unit with the same binary value.
* Code points in range from `U+10000` `to 10FFFF` are encoded with a double 2-byte unit called a *surrogate pair*.
* Code points in range from `U+D800` to `U+DFFF` are reserved in Unicode as surrogate pairs.
  They will never be reassigned and must only be used for implementation of UTF-16 encoding.

Code points above `U+10000` are encoded as follows:

1. `0x10000` is subtracted from the code point number.
2.  The result falls in range from `0x0` to `0x0FFFFF` and fits in 20 bits.
  It is split into two 10-bit parts (i.e. each one has value in range from `0x0` to `0x03FF`)
3. Top 10 bits are added to `0xD800`, so that `0xD800 ≤ result ≤ 0xDBFF`.
  The result is called a *high surrogate*.
4. Low 10 bits are added to `0xDC00`, so that `0xDC00 ≤ result ≤ 0xDFFF`.
  The result is called a *low surrogate*.
5. High and low surrogate together become the encoded value.

Ranges of encoded values for 2-byte BMP code points, high, and low surrogates do not intersect.
This allows self-synchronization on 2-byte units.

Using BOM for UTF-16 is recommended.
If a string is missing the BOM, the standard assumes it to be big-endian by default.

## UTF-8

UTF-8 has been developed as a more agile alternative to UTF-16 and UTF-32.
It is a variable-length encoding, using 1 to 4 bytes for each code point.
UTF-8 is also an extension of ASCII, with first 128 code points meaning the same characters.

UTF-8 uses 1 or more leading bits of each byte to define that byte's type.
Trailing bits are used to store the code point value.
In the following table the digits stand for 'markup' bits and dots show the space for the 'value' bits.

    bytes   meaning bits    byte 1      byte 2      byte 3      byte 4  
    1       7               0... ....   
    2       11              110. ....   10.. ....   
    3       16              1110 ....   10.. ....   10.. ....   
    4       21              1111 0...   10.. ....   10.. ....   10.. ....

In multi-byte sequences the first byte is called *leading* and the others are *trailing*.
An important part of UTF-8 design is that each value can be reliably recognized as a single-byte sequence, a leading byte or a trailing byte.

A sequence, that uses more bytes than required is called an *overlong encoding*.
An example of it would be `1100 0000 1010 0000` to encode `U+0020`.
Overlong encodings are technically possible but not allowed.

# Unicode-aware development

## Data flow and internal representation

A Unicode-aware application should be able to read and write to a number of encodings, including non-Unicode ones.
Indeed, a single encoding should be used for the internal text representation.
All text processing should be implemented and tested for this single encoding.


### UTF-16 as internal encoding

Pros:

* Used internally in Java and .Net platform.
* The default encoding for Windows.

Cons:

* Most existing texts use code points up to `U+007F`, where 2-byte storage is redundant.
* Handling different endianness and BOM adds extra complexity to string operations (e.g. splits and joins).
* Self-synchronized only on 2-byte units, but not on random bytes.
* Sorting by bytes doesn't mean sorting by code points.

### UTF-8 as internal encoding

Pros:

* Compact, which is beneficial for enormous documents. 
  (Although it may not be required to load the whole document into memory.)
* Most compatible with ASCII texts.
* Sorting by bytes does mean sorting by code points.
* Self-synchronizing on 1-byte units.
* Most texts in the internet are UTF-8.
* The default encoding for *NIX systems, including Linux and OS X.


## You don’t have to implement all from scratch

Good news, everyone!

For C/C++ and Java there is the [International Components for Unicode (ICU)](http://site.icu-project.org/).
It provides conversion with code pages, unicode normalization, case folding and other features.
The same may be available for any other programming languages and platforms.


## Myths and Caveats

There are some common misconceptions, which can lead to bugs.
They should be considered in development and testing of Unicode-aware programs.

* As said above, in most encodings a code point or a character does not always take two bytes to encode.
* Sorting strings lexicographically cannot be implemented by comparing code points or encoded values.
  It should also consider Unicode collation.
* A set of code points between the first and last letters of a given alphabet usually does not match that alphabet.
  This should be considered when using regular expressions.
* Case mappings are not always one-to-one.
  They can also be context and locale-dependent.
* There are left-to-right, right-to-left and top-to-bottom writings.
  These can even be used together in a single document.
* Some languages have 16-bit `char` types, which is not enough to store a code point.

## Invalid Unicode

Unicode input to the program can be and sometimes is broken.
Examples include invalid code points, erroneous multi-byte sequences, algorithmically impossible byte values, etc.
A number of security issues are known to be caused by improper handling of invalid Unicode.

Another common case is that using BOM with UTF-8 is not required and should be avoided.
However, lots of Windows applications do add `0xEFBBBF` in the beginning of a UTF-8 file.

## User experience

There are aspects of user experience with Unicode that should be considered.

* Some users cannot type letters with diacritical marks and do use plain ones in search.
  The program should employ Unicode normalization in this case.
  
* Cursor and backspace interactions with ligatures and combined characters are important.
  Often cursor treats ligature as a single character and backspace breaks it.
  (Particular UX decision is out of topic of this document.)


# About this document

The purpose of this document is to support the early stages of development of a “plaintext” editor. 
This document:

* Is an entry point to learning about Unicode;
* Provides language-agnostic information required to make a choice between different Unicode encodings for internal text representation;
* Mentions important corner-cases, which must be considered in design, development and quality assurance of a text editor.

Indeed, this document:

* Does not provide extensive and complete information about Unicode;
* Is not the development or test documentation;
* Does not aim to define all available Unicode-related terms.
